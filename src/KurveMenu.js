/**
 *
 * Program:     Kurve
 * Author:      Markus Mächler, marmaechler@gmail.com
 * License:     http://www.gnu.org/licenses/gpl.txt
 * Link:        http://achtungkurve.com
 *
 * Copyright © 2014, 2015 Markus Mächler
 *
 * Kurve is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kurve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kurve.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

'use strict';

Kurve.Menu = {
    
    boundOnKeyDown: null,
    
    init: function() {
		this.initMenuBase();
        this.initPlayerMenu();
    },
	
	initMenuBase: function() {
		var basePlayerMenu = "<td  id='menu1'>" +
			"<div id='menu'>" +
				"<div id='menu-players'>" +
					"<div class='head'>" +
						"<div class='title'>" +
							"<span>player</span>" +
						"</div>" +
						"<div class='key left'>" +
							"<span>left</span>" +
						"</div>" +
						"<div class='key right'>" +
							"<span>right</span>" +
						"</div>" +
						"<div class='superpower'>" +
							"<span>superpower</span>" +
						"</div>" +
					"</div>" +
					"<div class='clear'></div>" +

					"<div id='menu-players-list'></div>" +
				"</div>" +
			"</div>" +
		"</td>" +
		"<td id='menu2'>" +
			"<div id='menu' style='height: 620px;'>" +
				"<div id='menu-players'>" +
					"<div class='head'>" +
						"<div class='title'>" +
							"<span>player</span>" +
						"</div>" +
						"<div class='key left'>" +
							"<span>left</span>" +
						"</div>" +
						"<div class='key right'>" +
							"<span>right</span>" +
						"</div>" +
						"<div class='superpower'>" +
							"<span>superpower</span>" +
						"</div>" +
					"</div>" +
					"<div class='clear'></div>" +

					"<div id='menu-players-list2'></div>" +
				"</div>" +
			"</div>" +
		"</td>";
		
		document.getElementById("main-player-menu").innerHTML = basePlayerMenu;
		
		document.getElementById('menu2').style.display  = "none";
	},
        
    initPlayerMenu: function() {
        var playerHTML = '';
		
        Kurve.players.forEach(function(player) {
            playerHTML += player.renderMenuItem();
        });
        
        document.getElementById('menu-players-list').innerHTML = playerHTML;
    },
	
	addPlayerToMenu: function(player)
	{
		if(Kurve.players.length > 10){
			document.getElementById('menu2').style.display  = "";
			
			var playerMenu = document.getElementById('menu-players-list2');
		}
		else{
			var playerMenu = document.getElementById('menu-players-list');
		}

		playerMenu.innerHTML += player.renderMenuItem();
		var playerMenuItem = document.getElementById(player.getId());
		
		Kurve.fadeIn(playerMenuItem);
	},
	
	removePlayerFromMenu: function(player)
	{
		var playerMenuItem = document.getElementById(player.getId());
		Kurve.fadeOut(playerMenuItem);
	},
    
    startGame: function() {
		var nonActivePlayers = 0;
		Kurve.Game.curves = [];
        Kurve.players.forEach(function(player) {
            if ( player.isActive() ) {				
                Kurve.Game.curves.push(
                    new Kurve.Curve(player, Kurve.Game, Kurve.Field, Kurve.Config.Curve)
                );    
            }
			else{
				nonActivePlayers++;
			}
        });
        
        if (Kurve.Game.curves.length < 1 || nonActivePlayers > 0) {
            Kurve.Game.curves = [];
            return; //not enough players are ready
        }
        
		SendMessageToSenders(createJsonString("startGame", ""));
		
        u.addClass('hidden', 'layer-menu');
        u.removeClass('hidden', 'layer-game');
        
        Kurve.Game.startGame();
    },

    nextSuperpower: function(playerId) {
        var player = Kurve.getPlayer(playerId);
        var count = 0;
        var superpowerType = '';

        for (var i in Kurve.Superpowerconfig.types) {
            count++;
            if ( !(Kurve.Superpowerconfig.types[i] === player.getSuperpower().getType() ) ) continue;

            if ( Object.keys(Kurve.Superpowerconfig.types).length === count) {
                superpowerType = Object.keys(Kurve.Superpowerconfig.types)[0];
            } else {
                superpowerType = Object.keys(Kurve.Superpowerconfig.types)[count];
            }

            break;
        }
        player.setSuperpower( Kurve.Factory.getSuperpower(superpowerType) );
		SendMessageToSender(player.getId(), createJsonString("superpowerName", "" + player.getSuperpower().getLabel()));
    },

    previousSuperpower: function(playerId) {
        var player = Kurve.getPlayer(playerId);
        var count = 0;
        var superpowerType = '';

        for (var i in Kurve.Superpowerconfig.types) {
            count++;
            if ( !(Kurve.Superpowerconfig.types[i] === player.getSuperpower().getType() ) ) continue;

            if ( 1 === count) {
                superpowerType = Object.keys(Kurve.Superpowerconfig.types)[Object.keys(Kurve.Superpowerconfig.types).length - 1];
            } else {
                superpowerType = Object.keys(Kurve.Superpowerconfig.types)[count - 2];
            }

            break;
        }

        player.setSuperpower( Kurve.Factory.getSuperpower(superpowerType) );
		SendMessageToSender(player.getId(), createJsonString("superpowerName", "" + player.getSuperpower().getLabel()));
    },

    activatePlayer: function(playerId) {
        if ( Kurve.getPlayer(playerId).isActive() ) return;

        var player = Kurve.getPlayer(playerId);
		player.setIsActive(true);
		
        u.removeClass('inactive', playerId);
        u.addClass('active', playerId);
		
		document.getElementsByClassName(playerId + " active")[0].style.color = player.getColor();
    },

    deactivatePlayer: function(playerId) {
        if ( !Kurve.getPlayer(playerId).isActive() ) return;

		var player = Kurve.getPlayer(playerId);
		player.setIsActive(false);
		
        u.removeClass('active', playerId);
        u.addClass('inactive', playerId);
		
		document.getElementsByClassName(playerId + " inactive")[0].style.color = player.getColor();
    },

    togglePlayerActivation: function(playerId) {
        if ( Kurve.getPlayer(playerId).isActive() ) {
            Kurve.Menu.deactivatePlayer(playerId);
        } else {
            Kurve.Menu.activatePlayer(playerId);
        }
    },

    requestFullScreen: function() {
        document.body.webkitRequestFullScreen();
    }
    
};