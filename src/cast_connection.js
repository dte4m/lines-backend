/**
 *
 * Program:     Kurve
 * Author:      Arnout de Bruijn, info@shockingsheepstudios.nl
 * License:     http://www.gnu.org/licenses/gpl.txt
 * Link:        https://shockingsheepstudios.nl/gh-cf/dk
 *
 * Copyright © 2017, 2018 Arnout de Bruijn
 *
 * Kurve is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kurve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kurve.  If not, see <http://www.gnu.org/licenses/>.
 *
 */	
	
	window.onload = function() {
		cast.receiver.logger.setLevelValue(0);
		window.castReceiverManager = cast.receiver.CastReceiverManager.getInstance();
		console.log('Starting Receiver Manager');

		// handler for the 'ready' event
		castReceiverManager.onReady = function(event) {
		  console.log('Received Ready event: ' + JSON.stringify(event.data));
		  window.castReceiverManager.setApplicationState("Application status is ready...");
		};

		// handler for 'senderconnected' event
		castReceiverManager.onSenderConnected = function(event) {
			Kurve.addNewPlayer(event.senderId, "Player " + playerCounter, getPlayerColor());
			playerCounter++;
		};

		// handler for 'senderdisconnected' event
		castReceiverManager.onSenderDisconnected = function(event) {
			if(checkIfPlayerWasHost(Kurve.getPlayer(event.senderId))){
				assignNewHost();
			}
			removePlayer(event.senderId);
			
			console.log('Received Sender Disconnected event: ' + event.data);
			if (window.castReceiverManager.getSenders().length === 0) {
				window.close();
			}
		};

		// create a CastMessageBus to handle messages for a custom namespace
		window.messageBus =
		  window.castReceiverManager.getCastMessageBus(
			  'urn:x-cast:nl.shocking.sheep.studios.cast.lines');

		// handler for the CastMessageBus message event
		window.messageBus.onMessage = function(event) {
			handleMessage(event.data, event.senderId);
		};

		// initialize the CastReceiverManager with an application status message
		window.castReceiverManager.start({statusText: "Application is starting"});
		console.log('Receiver Manager started');
	};

	var LEFT = -1;
	var STRAIGHT = 0;
	var RIGHT = 1;	
	var colors = ['red','orange', 'green', 'blue', 'purple', 'pink', 'GreenYellow', 'aqua', 'azure', 'burlywood', 'plum', 'MediumTurquoise', 'DarkSalmon', 'Wheat', 'Silver', 'gold', 'Olive', 'DarkGoldenRod', 'AquaMarine', 'CadetBlue'];
	
	var BUTTON_CONTROLLS = "button"
	var MOTION_CONTROLLS = "motion"
	var controllsState = BUTTON_CONTROLLS
	
	var playerCounter = 1;
	
	function checkIfPlayerWasHost(player){
		return player.isHost();
	}
	
	function createJsonString(messageType, value){
		var employees = [];
		
		json = [];
		item = {};
		item["messageType"] = messageType;
		item["value"] = value;
		json.push(item);
		return JSON.stringify(json);
	}
	
	function assignNewHost()
	{
		try{
			if(Kurve.players.length > 0){
				Kurve.players.forEach(function(player) {
					if(!player.isHost()){							
						player.setIsHost(true);
				
						notifyNewHost(player.getId());
						throw BreakException;
					}
				});
			}
		}
		catch(exception)
		{
			// end of foreach
		}
	}
	
	function notifyNewHost(Id){
		SendMessageToSender(Id, SendMessageToSenders(createJsonString("newhost", "")));
	}
	
	function SendMessageToSender(Id, message){
        window.messageBus.send(Id, message);
    }
	
    function SendMessageToHost(playerId, message){
        window.messageBus.send(playerId, message);
    }
	
    function SendMessageToSenders(message){
        Kurve.players.forEach(function(player)
        {
            window.messageBus.send(player.getId(), message);
        });
    }
	
	function handleMessage(text, Id){
		var message = text;
		var appCommand = JSON.parse(message);
		
		if(text.indexOf("direction") > -1 )
		{
			handleDirection(Id, appCommand.direction[0].direction);
		}
		else if(text.indexOf("toggleReadyState") > -1)
		{
			handleToggleReadyState(Id);
		}
		else if (text.indexOf("playerName") > -1 )
		{
			var playerName = appCommand.playerName[0].playerName;
			//Assign first player as host.
			if(window.castReceiverManager.getSenders().length === 1){
				assignNewHost();
			}
			if(playerName !== ""){
				handleChangePlayername(playerName, Id);
			}
		}
		else if(text.indexOf("nextSuperpower") > -1 )
		{
			handleNextSuperpower(Id);
		}
		else if(text.indexOf("previousSuperpower") > -1 )
		{
			handlePreviousSuperpower(Id);
		}
		else if(text.indexOf("startGame") > -1 )
		{
			handleStartGame();
		}
		else if(text.indexOf("activateSuperPower") >-1 )
		{
			handleActivateSuperpower(Id);
		}
		else if(text.indexOf("startRound") > -1 )
		{
			handleStartRound();
		}
		else if(text.indexOf("togglePauseGame") > -1 )
		{
			handleTogglePauseGame();
		}
		else if(text.indexOf("toggleControlls") > -1 )
		{
			handleToggleControlls();
		}
	}
	
	function handleToggleReadyState(Id)
	{
		Kurve.Menu.togglePlayerActivation(Id)
	}
	function handleDirection(Id, direction)
	{
		switch(Id, direction)
		{
			case LEFT:
				Kurve.getPlayer(Id).setAngle(LEFT)
				break;
			case STRAIGHT:
				Kurve.getPlayer(Id).setAngle(STRAIGHT)
				break;
			case RIGHT:
				Kurve.getPlayer(Id).setAngle(RIGHT)
				break;
		}
		
	}
	
	function removePlayer(playerId)
	{
		Kurve.removePlayer(Kurve.getPlayer(playerId));
	}
	
	function handleTogglePauseGame()
	{
		Kurve.Game.togglePause();
	}
	
	function handleStartRound()
	{
		Kurve.Game.onStartRound();
	}
	
	function handleStartGame()
	{
		sendNewControllsMessage(controllsState);
		Kurve.Menu.startGame();
	}
	
	function handleChangePlayername(playername, Id){
		Kurve.getPlayer(Id).setPlayerName(playername);
	}
	
	function handleActivateSuperpower(Id)
	{
		Kurve.getPlayer(Id).setSuperpowerActivated(true);
	}
	
	function handleNextSuperpower(Id)
	{
		Kurve.Menu.nextSuperpower(Id);
	}
	
	function handlePreviousSuperpower(Id)
	{
		Kurve.Menu.previousSuperpower(Id);
	}
	
	function handleToggleControlls()
	{
		switch(controllsState)
		{
			case BUTTON_CONTROLLS:
				controllsState = MOTION_CONTROLLS;
				sendNewControllsMessage(MOTION_CONTROLLS);	
				swapControllsToMotionLayout();
				break;
			case MOTION_CONTROLLS:
				controllsState = BUTTON_CONTROLLS;
				sendNewControllsMessage(BUTTON_CONTROLLS);
				swapControllsToButtonLayout();
				break;
		}		
	}
	
	function swapControllsToButtonLayout(){
		document.querySelectorAll('.tiltleft').forEach(function(goLeftImage) {
			goLeftImage.style.backgroundImage = "url('resources/images/left_button_pressed.png')";
		});
		document.querySelectorAll('.tiltright').forEach(function(goRightImage) {
			goRightImage.style.backgroundImage = "url('resources/images/right_button_pressed.png')";
		});
		document.querySelectorAll('.shakephone').forEach(function(superpowerControlImage) {
			superpowerControlImage.style.backgroundImage = "url('resources/images/superpower_button.png')";
		});
	}
	
	function swapControllsToMotionLayout(){
		document.querySelectorAll('.tiltleft').forEach(function(goLeftImage) {
			goLeftImage.style.backgroundImage = "url('resources/images/tilt_left.png')";
		});
		document.querySelectorAll('.tiltright').forEach(function(goRightImage) {
			goRightImage.style.backgroundImage = "url('resources/images/tilt_right.png')";
		});
		document.querySelectorAll('.shakephone').forEach(function(superpowerControlImage) {
			superpowerControlImage.style.backgroundImage = "url('resources/images/shake.png')";
		});
	}
	
	function sendNewControllsMessage(newControlls)
	{		
		SendMessageToSenders(createJsonString("toggleControlls", newControlls));
	}
	
	function getPlayerColor()
	{
		if(colors.length > 0)
		{
			var color = colors[0];
			colors.shift();
			return color;
		}
		
		return "black";
	}