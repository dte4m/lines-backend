/**
 *
 * Program:     Kurve
 * Author:      Markus Mächler, marmaechler@gmail.com
 * License:     http://www.gnu.org/licenses/gpl.txt
 * Link:        http://achtungkurve.com
 *
 * Copyright © 2014, 2015 Markus Mächler
 *
 * Kurve is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kurve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kurve.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

'use strict';

var Kurve = {
    
    players: [],
    playersById: {},
    
    init: function() {
        this.Field.init();
		this.FieldPlayerPosition.init();
        this.Menu.init();
        this.Game.init();
        this.Lightbox.init();
    },

    getPlayer: function(playerId) {
        return this.playersById[playerId];
    },

    reload: function() {
        location.reload();
    },
	
	addNewPlayer: function(id, playerName, color, keyLeft, keyRight, keySuperpower)
	{
		var player = new Kurve.Player(id, playerName, color, keyLeft, keyRight, keySuperpower);

		Kurve.players.push(player);
		Kurve.playersById[player.getId()] = player;
		
		Kurve.Menu.addPlayerToMenu(player);
	},
	
	removePlayer: function(playerToRemove)
	{
		//make the removed player's color available for another person	
		colors.splice(0,0, playerToRemove.getColor());
		
		Kurve.players = Kurve.players.filter(function(player) {			
			return player.getId() !== playerToRemove.getId();
		});
		
		Kurve.Menu.removePlayerFromMenu(playerToRemove);
	},

    onUnload: function() {
    },
	
	//jQuery is not supported by chromecast for some reason.
	fadeOut: function(element) {
		var op = 1;  // initial opacity
		var timer = setInterval(function () {
			if (op <= 0.1){
				clearInterval(timer);
				element.style.display = 'none';
			}
			element.style.opacity = op;
			element.style.filter = 'alpha(opacity=' + op * 100 + ")";
			op -= op * 0.1;
		}, 20);
	},
	
	fadeIn: function(element) {
		//var op = 0.1;  // initial opacity
		//element.style.opacity = 0.1;
		element.style.display = 'block';
		/*var timer = setInterval(function () {
			if (op >= 1){
				clearInterval(timer);
			}
			element.style.opacity = op;
			element.style.filter = 'alpha(opacity=' + op * 100 + ")";
			op += op * 0.1;
		}, 10);*/
	}

};

document.addEventListener('DOMContentLoaded', Kurve.init.bind(Kurve));
