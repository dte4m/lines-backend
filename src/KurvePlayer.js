/**
 *
 * Program:     Kurve
 * Author:      Markus Mächler, marmaechler@gmail.com
 * License:     http://www.gnu.org/licenses/gpl.txt
 * Link:        http://achtungkurve.com
 *
 * Copyright © 2014, 2015 Markus Mächler
 *
 * Kurve is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kurve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kurve.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

'use strict';

Kurve.Player = function(id, playerName, color) {

    var points = 0;
    var superpower = Kurve.Factory.getSuperpower(Kurve.Superpowerconfig.types.RUN_FASTER);
    var superPowerElement = null;
	//To cross walls when the player is discovering his start position.
	var tempSuperPower = Kurve.Factory.getSuperpower("CROSS_WALLS_TEMP");
    var isActive = false;
	var angle = STRAIGHT;
	var superpowerActivated = false;
	var isHost = false;
    
    this.incrementPoints = function() {
        points++;
		SendMessageToSender(this.getId(), createJsonString("playerPoints", "" + points));
    };

    this.setSuperpower = function(newSuperpower) {
        superpower = newSuperpower;

        if ( superPowerElement === null ) {
            superPowerElement = document.getElementById(this.getId() + '-superpower');
        }

        superPowerElement.innerHTML = this.getSuperpower().getLabel();
    };

    this.setColor = function(newColor) { color = newColor; };
    this.setIsActive = function(newIsActive) { isActive = newIsActive; };
	this.setAngle = function(newAngle) { angle = newAngle; };
	this.setSuperpowerActivated = function(newSuperpowerActivated) { superpowerActivated = newSuperpowerActivated; };
	this.setIsHost = function(newIsHost) { isHost = newIsHost; };
	
    this.getPoints = function() { return points; };
    this.getId = function() { return id; };
    this.getColor = function() { if(isActive){ return color; } else{ return "#969696"; } };
	this.getPlayerName = function() { return playerName; };
    this.getSuperpower = function() { return superpower; };
	this.getTempSuperpower = function() { return tempSuperPower; };
    this.isActive = function() { return isActive; };
	this.getAngle = function() { return angle; };
	this.isHost = function() { return isHost; };
	
	this.getSuperpowerActivated = function() 
	{ 
		if(superpowerActivated)
		{ 
			superpowerActivated = false;
			return true;
		} 
		return superpowerActivated; 
	};
	
	this.setPlayerName = function(newPlayerName)
	{
		playerName = newPlayerName;
		document.getElementById('playerName'+this.getId()).innerHTML = "<h2>" + playerName + "</h2>";
	};

};

Kurve.Player.prototype.renderMenuItem = function() {
    return  '<div id="' + this.getId() + '" class="player inactive hidden ' + this.getId() +'">' +
                '<div id="playerName' + this.getId() + '" class="title light" style="overflow: hidden;"><h2>' + this.getPlayerName() + '</h2></div>' +
                '<div class="key left light"><div class="tiltleft"></div></div>' +
                '<div class="key right light"><div class="tiltright"></div></div>' +
                '<div class="superpower">' +
                    '<div class="shakephone key light"></div>' +
                    '<div class="superpowerType light">' +
                        '<div class="left"></div>' +
                        '<div class="superpowers">' +
                            '<div id="' + this.getId() + '-superpower">' + this.getSuperpower().getLabel() + '</div>' +
                        '</div> ' +
                        '<div class="right"></div>' +
                    '</div> ' +
                '</div>' +
                '<div class="clear"></div>' +
            '</div>';
};

Kurve.Player.prototype.renderScoreItem = function() {
    return  '<div class="active ' + this.getId() + '" style="color:'+ this.getColor() + ';">' +
                '<div class="title"><h2>' + this.getPlayerName()+ ":  " + this.getPoints() +'</h2></div>' +
                '<div class="superpowers">' + this.renderNumberOfSuperPowers() + '</div>' +
            '</div>';
};

Kurve.Player.prototype.renderNumberOfSuperPowers = function() {
    var superpowers = '';

    for (var i=0; i < this.getSuperpower().getCount(); i++ ) {
        superpowers += '<div class="superpowerCircle ' + this.getId() + '"></div>';
    }

    return superpowers;
};