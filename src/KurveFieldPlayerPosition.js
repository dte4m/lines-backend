/**
 *
 * Program:     Kurve
 * Author:      Markus Mächler, marmaechler@gmail.com
 * License:     http://www.gnu.org/licenses/gpl.txt
 * Link:        http://achtungkurve.com
 *
 * Copyright © 2014, 2015 Markus Mächler
 *
 * Kurve is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kurve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kurve.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

'use strict';

Kurve.FieldPlayerPosition = {
    
    canvas:         null,
    ctx:            null,
    
    width:          null,
    height:         null,
    
    drawnPixels:    [],

    defaultColor:       null,
    defaultLineWidth:   null,

    drawnPixelPrecision: null,
    
    init: function() {
        this.initCanvas();
        this.initContext();
        this.initDrawing();
        this.initField();
    },
    
    clearCanvas: function() {
		this.ctx.clearRect(0, 0, this.width, this.height);
	},
	
    initCanvas: function() {
        var width           = window.innerWidth * Kurve.Config.Field.width;
        var height          = window.innerHeight;
        
        this.canvas         = document.getElementById('field2');
        this.canvas.width   = width;
        this.canvas.height  = height;
        this.width          = width;
        this.height         = height;
    },
    
    initContext: function() {
        this.ctx = this.canvas.getContext('2d');
    },
    
    initField: function() {
        this.drawField();
    },

    initDrawing: function() {
        this.defaultColor = Kurve.Config.Field.defaultColor;
        this.defaultLineWidth = Kurve.Config.Field.defaultLineWidth;
        this.drawnPixelPrecision = Kurve.Config.Field.drawnPixelPrecision;
    },

    drawField: function() {
        this.ctx.beginPath();

        this.ctx.strokeStyle = Kurve.Config.Field.borderColor;
        this.ctx.lineWidth   = 3;
        
        Kurve.FieldPlayerPosition.clearCanvas();
        this.ctx.rect(0, 0, this.width, this.height);
      
        this.ctx.stroke();
        
        this.drawnPixels = [];
    },

    drawUntrackedPoint: function(pointX, pointY, color) {
        if ( color === undefined ) color = this.defaultColor;
		
        this.ctx.beginPath();
        this.ctx.fillStyle = color;
        this.ctx.arc(pointX, pointY, 2, 0, 2 * Math.PI, false);
        this.ctx.fill();
    }

};
