/**
 *
 * Program:     Kurve
 * Author:      Markus Mächler, marmaechler@gmail.com
 * License:     http://www.gnu.org/licenses/gpl.txt
 * Link:        http://achtungkurve.com
 *
 * Copyright © 2014, 2015 Markus Mächler
 *
 * Kurve is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kurve is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kurve.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

'use strict';

Kurve.Config = {

    Debug: {
        curvePosition: false,
        curveTrace: false,
        fieldDrawnPixels: false,
    },
    
    Field: {
        defaultColor: '#333',
        defaultLineWidth: 4,
        borderColor: '#ACAC9D',
        width: 1, //100% percent of the screen
    },
    
    Curve: {
        stepLength: 2.0,
        lineWidth: 4,
        dAngle: 0.07,
        holeInterval: 150,
        selfCollisionTimeout: 150,
		holeSize: 10,
		discoverTime: 50,
    },
    
    Game: {
        startDelay: 2000,
        fps: 30,
    },

    Utility: {
        interpolatedPixelsPrecision: 100,
    }

};